Description:

The downloadable file contains all data and associated scripts that generate results seen in the article.

Size:

Data – 656mb
Scripts – 324kb

Platform:

Microsoft Excel
R Programming (RStudio)
Environment:
Compatible with Mac OSX and Windows
All libraries provided in script

Major Component Description:

Data: contains three folders, each pertaining to Phase 1, 2, and 3 (Outdoor,
Indoor, UAV).

• Phase 1 and Phase 2 folders contain the ground truth dataset
associated with the calibration phase, and data pertaining to either the
NO2A43F (Old) sensor or the NO2AE (NEW) sensor in .csv and .xlsm
format. The Old and New sensor folders contain the raw data as
collected in the field. Unfiltered and Filtered_Averaged folders contain
files that have removed unnecessary data and are used within the
scripting environment.

• The UAV folder contains the raw data collected by the sensors and the
UAV flight log, which have been renamed and reorganized into a
Sensors and UAV folder in .csv and .xlsm format. There is also a UAV
experiment timeline in .txt format.
Scripts: contains 4 scripts in total.

• Phase 1 – 1 script for AE sensor (A43F sensor stopped working, so no
calibration conducted during this phase).

• Phase 2 – 1 script for the A43F sensor and 1 script for the AE sensor.

• Phase 3 – 1 script for both the A43F and AE sensor calibration.

Detailed Setup Instructions:

1. Download all associated data
2. Download RStudio if not already on system
3. Place Data folder in desirable location

Detailed Run Instructions:

1. Open script
2. Set appropriate file path in script to Data folder
3. Install all libraries in script
4. Run script while following comments

Output Description:

Time series and flight path plots, regression results including coefficients of determination (R2), root mean square errors, and calibrated sensor readings are derived from executing the provided scripts with associated data.

Contact Information:

In case of any questions, please contact the authors (mawrenceraphael@gmail.com; joao.valente@wur.nl).
